#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import six

def extract_blueprints_and_bundles():
    submodules = {}
    for name in ('index',):
        __import__('{}.{}'.format(__name__, name))
        submodules[name] = sys.modules['{}.{}'.format(__name__, name)]

    yield [submodule.blueprint for submodule in six.itervalues(submodules)]
    yield {
        '{}.{}'.format(name, key) if key.startswith('.') else key: value
        for name, submodule in six.iteritems(submodules)
        for key, value in submodule.bundles
    }

blueprints, bundles = extract_blueprints_and_bundles()
