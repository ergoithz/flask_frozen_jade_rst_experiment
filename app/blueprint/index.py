#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from flask import Blueprint, render_template, current_app

from ..extensions.flask_content import get_content

blueprint = Blueprint('index', __name__)
bundles = {}

@blueprint.route('/')
def index():
    page = get_content('index.rst')
    return render_template('index.jade', page=page)
