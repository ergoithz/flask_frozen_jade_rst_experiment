
import os
import os.path

def relative(path):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), path)

class BaseConfig(object):
    BUILD_PATH = relative('build')
    CONTENT_PATH = relative('content')
    PROPAGATE_EXCEPTIONS = True
