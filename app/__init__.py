#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import six

from flask import Flask
from flask_assets import Environment as AssetsEnvironment, ManageAssets
from flask_script import Manager

from .blueprint import blueprints, bundles
from .extensions import MarkupContent, JadeJinja
from .config import BaseConfig

def create_app(debug=False):
    app = Flask(__name__, template_folder='template')
    app.config.from_object(BaseConfig)
    if 'SITE_SETTINGS' in os.environ:
        app.config.from_envvar('SITE_SETTINGS')
    app.config['DEBUG'] |= True

    app.jinja_env.add_extension(JadeJinja)
    app.jinja_env.autoescape = False # enable pyjade unescaping

    assets = AssetsEnvironment(app)
    content = MarkupContent(app)

    @app.before_first_request
    def before_first_request():
        pass

    @app.context_processor
    def context_processor():
        return {
            'base': {
                'title': 'ithz'
            }
        }

    assets.register('base_css',
        'base.css',
        filters=None, output='{BUILD_PATH}/static/css'.format(**app.config)
        )
    assets.register('base_js',
        'base.js',
        filters=None, output='{BUILD_PATH}/static/js'.format(**app.config)
        )

    for name, bundle in six.iteritems(bundles):
        assets.register(name, bundle)

    for blueprint in blueprints:
        app.register_blueprint(blueprint)
    return app, assets

app, assets = create_app()
manager = Manager(app)
manager.add_command("assets", ManageAssets(assets))

if __name__ == '__main__':
    manager.run()
