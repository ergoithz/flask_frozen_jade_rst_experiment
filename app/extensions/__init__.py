
from pyjade.ext.jinja import PyJadeExtension as JadeJinjaBase

from .flask_content import MarkupContent

class JadeJinja(JadeJinjaBase):
    options = {
        'pretty': False
    }
