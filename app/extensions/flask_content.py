
from flask import current_app
from pelican.settings import DEFAULT_CONFIG as base_config
from pelican.readers import Readers

class MarkupContent(object):
    config_prefix = 'MARKUP_'
    defaults = dict(base_config)

    def __init__(self, app=None):
        if not app is None:
            self.init_app(app)

    def init_app(self, app):
        if not hasattr(app, 'extensions'):
            app.extensions = {}
        app.extensions['content'] = self

        settings = dict(self.defaults)

        prefix = self.config_prefix
        prefix_len = len(prefix)
        settings.update((key[prefix_len:], value)
                        for key, value in app.config.items()
                        if key.startswith(prefix))

        self.readers = Readers(settings=settings, cache_name='')

def get_content(path):
    extension = current_app.extensions['content']
    base_path = current_app.config['CONTENT_PATH']
    return extension.readers.read_file(
        base_path, path, content_class=dict, fmt=None,
        context=None, preread_signal=None, preread_sender=None,
        context_signal=None, context_sender=None)
